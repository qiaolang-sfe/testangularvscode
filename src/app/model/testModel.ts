export class DonutChartModel {
    dimension: number = 0;
    innerRadius: number = 0;
    backgroundClass: string = '';
    backgroundOpacity: number = 0;
}

type DonutChartModel2 = {
    dimension: number;
    innerRadius: number;
};

interface IDonutChartModel3 {
    dimension: number;
    innerRadius: number;
}

var donut: IDonutChartModel3 = {
    dimension: 1,
    innerRadius: 2
};